NAME
  qrouter - Multi-level, over-the-cell maze router

SYNOPSIS
  qrouter [-noc] [-s scriptname] [options] [design_name]

DESCRIPTION
  This manual page documents briefly the qrouter command.
  
  Qrouter is a tool to generate metal layers and vias to physically connect together a netlist in a VLSI fabrication technology. It is a maze router, otherwise known as an "over-the-cell" router or "sea-of-gates" router. That is, unlike a channel router, it begins with a description of placed standard cells, usually packed together at minimum spacing, and places metal routes over the standard cells.
  
  Qrouter uses the open standard LEF and DEF formats as file input and output. It takes the cell definitions from a LEF file, and analyzes the geometry for each cell to determine contact points and route obstructions. It then reads the cell placement, pin placement, and netlist from a DEF file, performs the detailed route, and writes an annotated DEF file as output.

USAGE

  Qrouter can run be run in several ways:

  1. Interactive mode. If qrouter is started without any options, a tkcon window will be opened with an interpreter where commands may be entered.

  2. Normal batch mode. This mode is activated if a tcl script is given with the "-s" option. It can be run with or without a graphical window. If "-noc" is given, no graphical window will appear.

    Run without graphical window:
     qrouter -noc -s routescript.tcl

    Run with a graphical window showing the layout while it is routing:
     qrouter -s routescript.tcl

  3. Compatibility mode with qrouter 1.1. This mode is activated if either of the options "-c", "-v", "-i", "-p" or "-g" are given. This is a batch mode.

OPTIONS

 -noc             No console mode

 -s <scriptname>  Run scriptname

COMPATIBILITY MODE
 -c <file>   Configuration file name (if not route.cfg)

 -v <level>  Verbose output level

 -i <name>   Print route names and pitches and exit

 -p <name>   Specify global power bus name

 -g <name>   Specify global ground bus name


INPUT
 The input files for qrouter is an unrouted or partly routed .def file containing the
 layout and a .cfg file containing the tcl script used to do the routing. The .cfg
 file also needs to specify a .lef file describing the technology.


OUTPUT
 qrouter will output the fully routed .def file (if successful routing). This may
 then later be converted to a GDSII file using tools such as magic.

SEE ALSO
 qflow(1)

AUTHOR
 qrouter was written by Tim Edwards <tim@opencircuitdesign.com>.

 This manual page was written by Ruben Undheim <ruben.undheim@gmail.com>, for the
 Debian project (and may be used by others).

